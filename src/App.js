import './App.css';
import Header from './components/Header'
import Grid from './components/Grid'
import axios from 'axios'
import Filter from './components/Filter'
import { useEffect, useState } from 'react'

function App() {
  const [products, setProducts] = useState([]) // to get all products
  const [allSizes, setAllSizes] = useState([]) // to get a set of values for the size dropdown
  const [priceCategory, setPriceCategory] = useState("Low"); // to get the chosen price sort to filter with
  const [sizeCategory, setSizeCategory] = useState("") // to get the chosen size to filter with

  const tempProducts = [...products] // create messy sandbox version of all products to filter with

  const filteredProducts = sizeCategory ?
    tempProducts.filter(item => item.sizes.includes(sizeCategory)) :
    tempProducts

  const sortedProducts = priceCategory === "Low" ?
    filteredProducts.sort((a, b) => a.price - b.price) :
    filteredProducts.sort((a, b) => b.price - a.price)

  useEffect(() => {
    const fetchItems = async () => {
      let { data } = await axios.get('/products.json')
      setProducts(data.products)
      let tempSizes = [...new Set(data.products.map(item => item.sizes).flat())]
      setAllSizes(tempSizes)
    }
    fetchItems();
  }, [])

  return (
    <div className="container">
      <Header />
      <Filter allSizes={ allSizes } products={products} setPriceCategory={setPriceCategory} setSizeCategory={ setSizeCategory }/>
      <Grid products={ sortedProducts }/>
    </div>
  );
}

export default App;
