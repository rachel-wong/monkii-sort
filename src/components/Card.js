import React from 'react'

const Card = ({ product }) => {
  const {name, image, price } = product
  return (
    <div className="card">
      <img className="card-image" src={image} alt={product.name} />
      <h3 className="card-title">{name}</h3>
      <p className="card-price">${price}</p>
    </div>
  )
}

export default Card
