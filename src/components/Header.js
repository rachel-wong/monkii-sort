import React from 'react'
import Logo from '../assets/branding/monkii-logo.png'
const Header = () => {
  return (
    <div className="header">
      <img src={ Logo } alt="Monkii shirts" />
    </div>
  )
}

export default Header
