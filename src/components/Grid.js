import React from 'react'
import Card from './Card'

const Grid = ({ products }) => {
  if (!products ) return <div>Loading ...</div>
  return (
    <div className="grid-wrapper">
      <div className="grid">
        {products.map((item, idx) => (
          <Card key={idx} product={ item } />
        ))}
      </div>
    </div>
  )
}

export default Grid
