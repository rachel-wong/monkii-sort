import React from 'react'

const Filter = ({ products, setSizeCategory, setPriceCategory, allSizes }) => {


  return (
    <div className="filter-wrapper">
      <div className="filter-counter">
        <h1>Products</h1>
        <p>{ products.length > 1 ? products.length + " items" : products.lenth + " item" }</p>
      </div>
      <div className="filters">
        <div className="filter__inner">
          <label className="filter_label" htmlFor="price">Order By:</label>
          <select onChange={(e) => setPriceCategory(e.currentTarget.value)} className="filter_select" id="price">
            <option value="Low">Price - Low to High</option>
            <option value="High">Price - High to Low</option>
          </select>
        </div>
        <div className="filter__inner">
          <label className="filter_label" htmlFor="sizes">Filter by:</label>
          <select className="filter_select" id="sizes" onChange={(e) => setSizeCategory(e.target.value)}>
            {allSizes.map((size, idx) => (
              <option key={idx} value={size}>{ size }</option>
            ))}
          </select>
        </div>
      </div>
    </div>
  )
}

export default Filter
